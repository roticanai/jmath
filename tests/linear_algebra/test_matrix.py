# -*- coding: utf-8 -*-

import pytest
from jmath import linear_algebra as la

def test_matrix_isEmpty():
    empty = la.Matrix(4,2)
    non_empty = la.Matrix(10,10)
    la.mark_diagonal(non_empty, value=10)

    empty.isEmpty() == True
    non_empty.isEmpty == False

def test_matrix_multiplication():

    A = la.Matrix(3,3)
    A._matrix[0][0] = 1
    A._matrix[1][0] = -2
    A._matrix[1][1] = 1
    A._matrix[2][2] = 1

    B = la.Matrix(3,3)
    B._matrix[0][0] = 2
    B._matrix[0][1] = 4
    B._matrix[0][2] = -2
    B._matrix[1][0] = 4
    B._matrix[1][1] = 9
    B._matrix[1][2] = -3
    B._matrix[2][0] = -2
    B._matrix[2][1] = -3
    B._matrix[2][2] = 7

    C = la.Matrix(3,3)
    C._matrix[0][0] = 2
    C._matrix[0][1] = 4
    C._matrix[0][2] = -2
    C._matrix[1][1] = 1
    C._matrix[1][2] = 1
    C._matrix[2][0] = -2
    C._matrix[2][1] = -3
    C._matrix[2][2] = 7

    D = la.Matrix(3,3)
    la.mark_diagonal(D, value=1)
    D._matrix[1][0] = -4

    I = la.Matrix(3,3)
    la.mark_diagonal(I, value=1)

    NI = la.Matrix(3,3)
    la.mark_diagonal(NI, value=-1)

    # tests
    assert A * B == C
    assert A * A == D

    # test identity matrix multiplication AI = I
    assert A * I == A
    assert B * I == B
    assert (A * A) * I == D
    assert (A * I) * A == D
    assert (I * -1) == NI
    assert (-1 * I) == NI
    assert 2 * (-2 * A) == -4 * A
    assert 10 * A - (5 * A) == 5 * A

def test_matrix_addition():

    A = la.Matrix(5,5)
    la.mark_diagonal(A, value=1)

    B = la.Matrix(5,5)
    la.mark_diagonal(B, value=2)

    C = la.Matrix(2,2)
    C._matrix[0][0] = 1
    C._matrix[0][1] = 2
    C._matrix[1][0] = 3
    C._matrix[1][1] = 4

    D = la.Matrix(2,2)
    D._matrix[0][0] = 2
    D._matrix[0][1] = 2
    D._matrix[1][0] = 3
    D._matrix[1][1] = 5

    E = la.Matrix(2,2)
    la.mark_diagonal(E, value=1)

    assert A + A == B
    assert A + B == A * 3
    assert C + E == D

def test_matrix_subtraction():

    B = la.Matrix(3,3)
    B._matrix[0][0] = 2
    B._matrix[0][1] = 4
    B._matrix[0][2] = -2
    B._matrix[1][0] = 4
    B._matrix[1][1] = 9
    B._matrix[1][2] = -3
    B._matrix[2][0] = -2
    B._matrix[2][1] = -3
    B._matrix[2][2] = 7

    assert 10 * B - (5 * B) == 5 * B
    assert B - B == 0

def test_matrix_transpose():

    # test transpose square matrix
    B = la.Matrix(2,2)
    B._matrix[0][0] = 1
    B._matrix[0][1] = 2
    B._matrix[1][0] = 3
    B._matrix[1][1] = 4

    C = la.Matrix(2,2)
    C._matrix[0][0] = 1
    C._matrix[0][1] = 2
    C._matrix[1][0] = 3
    C._matrix[1][1] = 4

    D = la.Matrix(2,2)
    D._matrix[0][0] = 1
    D._matrix[0][1] = 3
    D._matrix[1][0] = 2
    D._matrix[1][1] = 4

    assert C.transpose() == D
    assert D.transpose() == C.transpose() == B

    # test transpose non-square matrix
    A = la.Matrix(2,3)
    A._matrix[0][0] = 1
    A._matrix[0][1] = 2
    A._matrix[0][2] = 3
    A._matrix[1][0] = 4
    A._matrix[1][1] = 5
    A._matrix[1][2] = 6

    B = la.Matrix(3,2)
    B._matrix[0][0] = 1
    B._matrix[0][1] = 4
    B._matrix[1][0] = 2
    B._matrix[1][1] = 5
    B._matrix[2][0] = 3
    B._matrix[2][1] = 6

    assert A.transpose() == B
    assert A.transpose() == B.transpose()

