# -*- coding: utf-8 -*-
"""put module custom exceptions here"""

__all__ = ["NonSquareMatrixError", "MatrixShapeMismatchError", "MultiplicationError"]

class NonSquareMatrixError(Exception): pass

class MatrixShapeMismatchError(Exception): pass

class MultiplicationError(Exception): pass
