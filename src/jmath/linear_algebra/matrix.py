# -*- coding: utf-8 -*-
"""Matrix"""

from .utils import create_zero_matrix
from .exceptions import *

class Matrix(object):
    """A Matrix Object
    """

    def __init__(self, row, column):
        self.row = row
        self.column = column
        self._matrix = create_zero_matrix(row, column)

    def transpose(self):
        """transpose a matrix

            if matrix is square, swap components in place between upper and lower triangle
            if matrix is not square, loop through entire matrix and create transpose in a new matrix
        """
        if self.isSquare():
            for i in range(self.row):
                for upper_diagnonal_column in range(i + 1, self.column):
                    upper = self._matrix[i][upper_diagnonal_column]
                    self._matrix[i][upper_diagnonal_column] = self._matrix[upper_diagnonal_column][i]
                    self._matrix[upper_diagnonal_column][i] = upper
        else:
            _temp = create_zero_matrix(self.column, self.row)
            for i in range(self.row):
                for j in range(self.column):
                    _temp[j][i] = self._matrix[i][j]
            _row = self.column
            _column = self.row
            self._matrix = _temp
            self.column = _column
            self.row = _row

        return self

    def isSquare(self):
        return self.column == self.row

    def isEmpty(self):
        from itertools import chain
        return sum(chain.from_iterable(row for row in self._matrix)) == 0

    def __mul__(self, other):
        """define multiplication operator

            there must be a better way of doing this
        """
        from .utils import dot_product
        from copy import deepcopy

        if isinstance(other, self.__class__):
            # matrix multiplication
            _temp = Matrix(self.row, other.column)
            if self.column != other.row:
                raise MultiplicationError("LHS column size must be same as RHS row size")
            _other = deepcopy(other)
            _other.transpose() # transpose to treat other's row as column for dot_product to work
            for i in range(self.row):
                for j in range(other.column):
                    _temp._matrix[i][j] = dot_product(self._matrix[i], _other._matrix[j])
            del _other
            return _temp
        elif isinstance(other, int):
            # scalar multiplication
            _temp = Matrix(self.row, self.column)
            for i in range(self.row):
                for j in range(self.column):
                    _temp._matrix[i][j] = self._matrix[i][j] * other
            return _temp
        else:
            raise TypeError("unsupported operand type(s) for *: '{}' and '{}'".format(self.__class__, type(other)))

    __rmul__ = __mul__

    def __add__(self, other):
        """define addition operator

            sum over each component of both iterables for each row.
            This is possible because both matrix must be of the same shape.

            map(function, iterable1, iterable2 , ...) according to docs will
            process items from all iterables in parallel. Given that,
            this implementation should be faster than naive nested for-loops
        """
        try:
            if self.row != other.row or self.column != other.column:
                raise MatrixShapeMismatchError("add operation required both matrices to be same shape")

            a = self._matrix
            b = other._matrix
            result = [ list(map(lambda x,y: x + y, a[i], b[i])) for i in range(self.row) ]
            _temp = Matrix(len(result), len(result[0]))
            _temp._matrix = result
        except:
            raise TypeError("unsupported operand type(s) for +: '{}' and '{}'".format(self.__class__, type(other)))
        else:
            return _temp

    def __sub__(self, other):
        """define substration operator
        """
        try:
            if self.row != other.row or self.column != other.column:
                raise MatrixShapeMismatchError("add operation required both matrices to be same shape")

            a = self._matrix
            b = other._matrix
            result = [ list(map(lambda x,y: x - y, a[i], b[i])) for i in range(self.row) ]
            _temp = Matrix(len(result), len(result[0]))
            _temp._matrix = result
        except:
            raise TypeError("unsupported operand type(s) for -: '{}' and '{}'".format(self.__class__, type(other)))
        else:
            return _temp

    def __getitem__(self, key):
        """define matrix getter
        """
        return self._matrix[key]

    def __setitem__(self, key, value):
        """define matrix setter
        """
        if isinstance(value, list) is False: raise TypeError("matrix row must be of type list")
        if len(value) != self.column: raise MatrixShapeMismatchError("all matrix row must have same # of columns (i.e. components)")
        self._matrix[key] = value

    def __iter__(self):
        """just return a generator
        """
        for row in self._matrix: yield row

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if self.row != other.row or self.column != other.column: return False
            return all(self._matrix[i][j] == other._matrix[i][j]
                            for j in range(self.column)
                            for i in range(self.row))
        elif other == 0:
            return self.isEmpty()
        else:
            return False

    def __str__(self):
        return "\n".join(map(str, self._matrix))

    def __repr__(self):
        return self.__str__()

    def __call__(self):
        return self._matrix
