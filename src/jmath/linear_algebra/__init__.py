"""linear algebra submodule
"""

from .matrix import Matrix
from .utils import *

__all__ = ["Matrix"] + utils.__all__
