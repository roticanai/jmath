# -*- coding: utf-8 -*-
"""put module utils here"""

from .exceptions import NonSquareMatrixError

__all__ = ["create_zero_matrix", "mark_diagonal", "mark_triangle",
            "dot_product"]

def create_zero_matrix(row, column):
    """init a matrix of Row x Column with zero components
    """
    if all(isinstance(arg, int) for arg in [row,column]) is False:
        raise TypeError("row and column must be type int")
    return [[0] * column for i in range(row)]

def mark_diagonal(matrix, value=1):
    """mark diagonal of square matrix with value [mutator]
    """
    if matrix.isSquare() is False:
        raise NonSquareMatrixError("non square matrix does not have diagonal")
    for i in range(matrix.row): matrix._matrix[i][i] = value

def mark_triangle(matrix, value=1, upper=True):
    """mark upper or lower triangle of matrix with value [mutator]
        default to upper triangle

        if upper: mark all components right of diagonal for each row
        if not upper: mark all components left of diagonal for each row
    """
    if matrix.isSquare() is False:
        raise NonSquareMatrixError("non square matrix does not have upper triangle")

    if upper:
        start = lambda x: x + 1
        stop = matrix.column
        step = 1
    else:
        start = lambda x: x - 1
        stop = -1
        step = -1

    for r in range(matrix.row):
        for c in range(start(r), stop, step):
            matrix._matrix[r][c] = value

def dot_product(a, b):
    """calculate the dot product between two iterables
    """
    if all(hasattr(arg, "__iter__") for arg in [a,b]) is False:
        raise TypeError("args for dot_product must both be iterable")
    return sum(map(lambda x,y: x * y, a, b))
