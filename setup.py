# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

from glob import glob
from os.path import basename
from os.path import join
from os.path import splitext

# configure setup py
setup(
  name='jmath',
  version='0.2.0',
  url='',
  author='jasonator',
  author_email='rotanosaj@elgoog.moc',
  license='BSD',
  description='some math stuff',
  packages=find_packages('src'),
  package_dir={'': 'src'},
  py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
  include_package_data=True,
  setup_requires=['pytest-runner'],
  tests_require=['pytest']
)
